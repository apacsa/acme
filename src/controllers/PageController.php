<?php
namespace Acme\Controllers;

use duncan3dc\Laravel\BladeInstance;

class PageController extends BaseController
{

    public function getShowHomePage()
    {
        //include(__DIR__.'/../../views/home.php');
        //$this->unsetVariables();
        //echo $this->twig->render('home.html');
        echo $this->blade->render("home", ['test' => 'again ']);
    }

    public function getShowAboutPage()
    {
        //include(__DIR__.'/../../views/home.php');
        //$this->unsetVariables();
        //echo $this->twig->render('about.html');
        echo $this->blade->render('about');
    }

}
