<?php
namespace Acme\Controllers;

use Acme\Models\User;
use Acme\Validation\Validator;
use duncan3dc\Laravel\BladeInstance;

class RegisterController extends BaseController
{

    public function getShowRegisterPage()
    {
        //include(__DIR__.'/../../views/register.html');
        //$this->unsetVariables();
        //echo $this->twig->render('register.html');
        echo $this->blade->render("register");
    }

    public function postShowRegisterPage()
    {
        $errors = [];

        $validation_data = [
          'first_name' => 'min:3',
          'last_name' => 'min:3',
          'email' => 'email|equalTo:verify_email',
          'verify_email' => 'email',
          'password' => 'min:3|equalTo:verify_password',
        ];

        //validate data
        $validator = new Validator();
        $errors = $validator->isValid($validation_data);

      if (count($errors) > 0) {
          //echo $this->twig->render('register.html', ['errors' => $errors]);
          $_SESSION['msg'] = $errors;
          echo $this->blade->render('register');
          unset($_SESSION['msg']);
          exit();
          //$_SESSION['errors'] = $errors;
          //$_SESSION['first_name'] = $_REQUEST['first_name'];
          //$_SESSION['last_name'] = $_REQUEST['last_name'];
          //$_SESSION['email'] = $_REQUEST['email'];
          //$_SESSION['verify_email'] = $_REQUEST['verify_email'];
          //header("Location: /register");
      } else {
          //else save data into the database
          $user = new User();
          $user->first_name = $_REQUEST['first_name'];
          $user->last_name = $_REQUEST['last_name'];
          $user->email = $_REQUEST['email'];
          $user->password = password_hash($_REQUEST['password'], PASSWORD_DEFAULT);
          $user->save();
          $this->unsetVariables();
          header("Location: /");
      }
    }

    public function getShowLoginPage()
    {
        //include(__DIR__.'/../../views/login.html');
        echo $this->blade->render("login");
    }

    private function unsetVariables()
    {
        unset($_SESSION['errors']);
        unset($_SESSION['first_name']);
        unset($_SESSION['last_name']);
        unset($_SESSION['email']);
        unset($_SESSION['verify_email']);
        unset($errors);
    }

}
