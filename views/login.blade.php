@extends('base')

@section('title')
Log In
@stop

@section('content')
<div class='row'>
  <div class='col-md-offset-2 col-md-8'>
    <h1>Log In</h1>
    <hr>
    <form  id='registerform' name='registerform' class='form-horizontal'>
      <div class="form-group">
        <label for="username" class='col-sm-2 control-label'>Email</label>
        <div class='col-sm-10'>
          <input type="email" class='form-control required email' id="username" name="username" placeholder='user@example.com'/>
        </div>
      </div>
      <div class="form-group">
        <label for="password" class='col-sm-2 control-label'>Password</label>
        <div class='col-sm-10'>
          <input type="password" class='form-control ' id="password" name="password" placeholder='Password'/>
        </div>
      </div>
      <hr>
      <div class="form-group">
        <div class='col-sm-offset-2 col-sm-10'>
          <button type="submit" class='btn btn-primary'>Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>
@stop
