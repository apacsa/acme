@extends('base')

@section('title')
Register
@stop

@section('content')
<div class='row'>
  <div class='col-md-offset-2 col-md-8'>
    <h1>Register</h1>
    <hr>
    @include('errormessage')
    <form  id='registerform' name='registerform' class='form-horizontal' action='/register' method='post' novalidate>
      <div class="form-group">
        <label for="first_name" class='col-sm-2 control-label'>First Name</label>
        <div class='col-sm-10'>
          <input type="text" class='form-control' id="first_name" name="first_name" placeholder='John' required />
        </div>
      </div>
      <div class="form-group">
        <label for="last_name" class='col-sm-2 control-label'>Last Name</label>
        <div class='col-sm-10'>
          <input type="text" class='form-control' id="last_name" name="last_name" placeholder='Smith' required/>
        </div>
      </div>
      <div class="form-group">
        <label for="email" class='col-sm-2 control-label'>Email</label>
        <div class='col-sm-10'>
          <input type="email" class='form-control' id="email" name="email" placeholder='user@example.com' required />
        </div>
      </div>
      <div class="form-group">
        <label for="verify_email" class='col-sm-2 control-label'>Verify Email</label>
        <div class='col-sm-10'>
          <input type="email" class='form-control' id="verify_email" name="verify_email" placeholder='user@example.com' required />
        </div>
      </div>
      <div class="form-group">
        <label for="password" class='col-sm-2 control-label'>Password</label>
        <div class='col-sm-10'>
          <input type="password" class='form-control' id="password" name="password" placeholder='Password' pattern="[a-zA-Z0-9]{3,15}" title="Passwords must contain at least 3 alphanumeric characters" required />
        </div>
      </div>
      <div class="form-group">
        <label for="verify_password" class='col-sm-2 control-label'>Verify Password</label>
        <div class='col-sm-10'>
          <input type="password" class='form-control' id="verify_password" name="verify_password" placeholder='Password' pattern="[a-zA-Z0-9]{3,15}" title="Passwords must contain at least 3 alphanumeric characters" required/>
        </div>
      </div>
      <hr>
      <div class="form-group">
        <div class='col-sm-offset-2 col-sm-10'>
          <button type="submit" class='btn btn-primary'>Register</button>
          <button type="reset" class='btn btn-danger'>Reset</button>
        </div>
      </div>
    </form>
  </div>
</div>
@stop

@section('bottomjs')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
<script>
/*
$(document).ready(function(){
  $('#registerform').validate({
    rules : {
      verify_email : {
        required : true,
        email : true,
        equalTo : "#email"
      }
      verify_password : {
        required : true,
        equalTo : "#password"
      }
    }
  });
});
*/
</script>
@stop
