<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content='width=device-width, initial-scale=1' />
  <title>Acme - @yield('title')</title>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  @yield('css')
</head>
<body>
  @include('topnav')
  @yield('outsidecontainer')
  <div class='container'>
    @yield('content')
  </div>
  @include('footer')
  <script src='//code.jquery.com/jquery-1.11.3.min.js'></script>
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
  @yield('bottomjs')
</body>
</html>
