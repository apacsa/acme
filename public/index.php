<?php

  include(__DIR__.'/../bootstrap/start.php');
  Dotenv::load(__DIR__.'/../');
  include(__DIR__.'/../bootstrap/db.php');
  include(__DIR__.'/../routes.php');
/*
  $url = $_SERVER['REQUEST_URI'];
  if ($url[1] = '') {
    //display home page
    include(__DIR__.'/../views/home.php');
    exit();
  } else if ($url[1] ) {
    # code...
  }
*/
/* with AltoRouter

$router->map('GET', '/', function(){
  include(__DIR__.'/../views/home.php');
});
$router->map('GET', '/login', function(){
  include(__DIR__.'/../views/login.php');
});
$router->map('GET', '/register', function(){
  include(__DIR__.'/../views/register.php');
});
$router->map('POST', '/', function(){
  include(__DIR__.'/../views/doRegister.php');
});
$match = $router->match();
if ($match && is_callable($match['target'])) {
  call_user_func_array($match['target'], $match['params']);
} else {
  header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
}
*/
$match = $router->match();
list($controller, $method) = explode('@', $match['target']);
if (is_callable(array($controller, $method))){
  $object = new $controller();
  call_user_func_array(array($object, $method), array($match['params']));
} else {
  echo "Cannot find $controller -> $method.";
}
